﻿using UnityEngine;
using System.Collections;

public class GrappleHook : MonoBehaviour {

	DistanceJoint2D joint;
	Vector3 targetPosition;
	RaycastHit2D raycast;
	public float distance = 10.0f;
	public LayerMask mask;
	public LineRenderer line;
	float step = 0.2f;

	// Use this for initialization
	void Start () {
		joint = GetComponent<DistanceJoint2D> ();
		joint.enabled = false;
		line.enabled = false;
	}

	// Update is called once per frame
	void Update () {

		if (joint.distance > 1.0f) {
			joint.distance -= step;
		} else {
			line.enabled = false;
			joint.enabled = false;
		}


		if (Input.GetKeyDown(KeyCode.E)) {
			targetPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			targetPosition.z = 0;

			raycast = Physics2D.Raycast (transform.position, targetPosition - transform.position, distance, mask);

			if (raycast.collider != null && raycast.collider.gameObject.GetComponent<Rigidbody2D>() != null) {
				joint.enabled = true;
				joint.connectedBody = raycast.collider.gameObject.GetComponent<Rigidbody2D>();

				//joint.connectedAnchor = raycast.point - new Vector2 (raycast.collider.transform.position.x, raycast.collider.transform.position.y); 

				Vector2 connectPoint =raycast.point - new Vector2(raycast.collider.transform.position.x, raycast.collider.transform.position.y);
				connectPoint.x = connectPoint.x / raycast.collider.transform.localScale.x;
				connectPoint.y = connectPoint.y / raycast.collider.transform.localScale.y;
				Debug.Log (connectPoint);
				joint.connectedAnchor = connectPoint;

				joint.distance = Vector2.Distance (transform.position, raycast.point);

				line.enabled = true;
				line.SetPosition (0, transform.position);
				line.SetPosition (1, raycast.point);
			}

		}

		if (Input.GetKey (KeyCode.E)) {
			line.SetPosition (0, transform.position);
		}

		if (Input.GetKeyUp(KeyCode.E)) {
			joint.enabled = false;
			line.enabled = false;
		}
		
}
}
