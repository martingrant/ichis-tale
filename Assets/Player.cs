﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public float speed;
	public bool grounded = true;
	public float jumpPower = 190;
	private bool hasJumped = false;
	public GameObject grappleHook;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {

		if(!grounded && GetComponent<Rigidbody2D>().velocity.y == 0) {
			grounded = true;
		}
			
		if (Input.GetAxis("Vertical") > 0 && grounded == true) {
			hasJumped = true;
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0,2), ForceMode2D.Impulse);
		}

		if (Input.GetAxis("Horizontal") < 0) {
			//transform.position += new Vector3(0, speed * Time.deltaTime, 0);
			//GetComponent<Rigidbody2D>().AddForce(new Vector2(0,1), ForceMode2D.Impulse);
		}

		//var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
		//transform.position += move * speed * Time.deltaTime;



		if (Input.GetAxis("Horizontal") < 0)
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		if (Input.GetAxis("Horizontal") > 0)
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.UpArrow))
		{
			//transform.position += Vector3.up * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.DownArrow))
		{
			//transform.position += Vector3.down * speed * Time.deltaTime;
		}

		if (Input.GetMouseButton(0)) {
			

			Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			pz.z = 0;
			//gameObject.transform.position = pz; 

			//grappleHook.transform.position = pz;
		}

	}

	void FixedUpdate () {
		if (hasJumped) {
			GetComponent<Rigidbody2D> ().AddForce (transform.up * jumpPower);
			grounded = false;
			hasJumped = false;
		}
	}
}